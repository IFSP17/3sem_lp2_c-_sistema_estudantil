﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Projeto
{
    class PessoaDAO
    {
        public static String tabela = "tbl_pessoa";
        public Pessoa pessoa { set; get; }


        public void criardb()
        {
            String SQL = "CREATE TABLE :tabela ("
                                + "registro serial primary key,"
                                + "nome	 varchar(30) not null,"
                                + "curso varchar(50) not null,"
                                + "idade int not null"
                            + ");";
            SQL = fazerSubstituicao(SQL);
            Console.WriteLine(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public void cadastrar()
        {
            if (pessoa == null) return;
            String SQL = "INSERT INTO :tabela (nome, curso, idade) VALUES "
                + "(':nome',':curso',':idade')";
            SQL = fazerSubstituicao(SQL);
            Console.WriteLine(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        public void alterar()
        {
            String SQL = "UPDATE :tabela SET nome=':nome', curso=':curso', "
                    + "idade=:idade WHERE registro=:registro";
            SQL = fazerSubstituicao(SQL);
            Console.WriteLine(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }

        private String fazerSubstituicao(String SQL)
        {
            SQL = SQL.Replace(":tabela", tabela);
            if (pessoa != null)
            {
                SQL = SQL.Replace(":registro", pessoa.registro.ToString());
                SQL = SQL.Replace(":nome", pessoa.nome);
                SQL = SQL.Replace(":curso", pessoa.curso);
                SQL = SQL.Replace(":idade", pessoa.idade.ToString());
            }
            return SQL;
        }

        public void consultar(int id)
        {
            String SQL = "SELECT * FROM :tabela WHERE registro=:registro";
            SQL = SQL.Replace(":registro", pessoa.registro.ToString());
            SQL = fazerSubstituicao(SQL);
            Console.WriteLine(SQL);
        }

        public List<Pessoa> listar()
        {
            String SQL = "SELECT registro,nome,curso,idade FROM :tabela";
            SQL = fazerSubstituicao(SQL);
            Console.WriteLine(SQL);
            Database db = new Database();
            db.abrir();
            NpgsqlCommand command = new NpgsqlCommand(SQL, db.conn);
            NpgsqlDataReader dr = command.ExecuteReader();
            List<Pessoa> lista = new List<Pessoa>();
            while (dr.Read())
            {
                Pessoa p = new Pessoa();
                p.registro = int.Parse(dr[0].ToString());
                p.nome = dr[1].ToString();
                p.curso = dr[2].ToString();
                p.idade = int.Parse(dr[3].ToString());
                lista.Add(p);
            }

            db.fechar();
            return lista;
        }

        public void excluir()
        {
            String SQL = "DELETE FROM :tabela WHERE registro=:registro";
            SQL = SQL.Replace(":tabela", tabela);
            SQL = SQL.Replace(":registro", pessoa.registro.ToString());
            Console.WriteLine(SQL);
            Database db = new Database();
            db.executaSQL(SQL);
        }
    }
}
