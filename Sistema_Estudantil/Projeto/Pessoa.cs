﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto
{
    class Pessoa
    {
            public int registro { set; get; }
            public string nome { set; get; }
            public string curso { set; get; }
            public int idade { set; get; }

            public override string ToString()
            {
                return registro.ToString() + " " + nome;
            }
        
    }
}
