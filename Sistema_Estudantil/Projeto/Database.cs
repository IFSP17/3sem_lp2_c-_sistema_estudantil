﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;

namespace Projeto
{
    class Database
    {
        public NpgsqlConnection conn;
        public void abrir()
        {
            conn = new NpgsqlConnection("Server=127.0.0.1;" +
                "User Id=postgres;Password=larissa123;Database=postgres;");
            // Abre a conexão
            conn.Open();
        }

        public int executaSQL(String tSQL)
        {
            int n = 0;
            try
            {
                abrir();
                NpgsqlCommand SQL = new NpgsqlCommand(tSQL, conn);
                n = SQL.ExecuteNonQuery();
                fechar();
            }
            catch (Exception erro)
            {
                Console.WriteLine(erro.Message);
            }
            return n;
        }

        public void fechar()
        {
            conn.Close();
        }
    }
}
