﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projeto
{
    public partial class Form1 : Form
    {
        private List<Pessoa> lista;

        public Form1()
        {
            InitializeComponent();
            lista = new List<Pessoa>();
            atualizaLista();
            
        }

        private void atualizaLista()
        {
            PessoaDAO dao = new PessoaDAO();
            lista = dao.listar();
            lstPessoa.DataSource = null;
            lstPessoa.DataSource = lista;

            //  lstCarros.DisplayMember = "modelo";
        }

        private Pessoa recolheDados()
        {
            try
            {
                Pessoa pessoa = new Pessoa();
                pessoa.nome = txtNome.Text;
                pessoa.idade = int.Parse(txtIdade.Text);
                pessoa.curso = txtCurso.Text;
                return pessoa;
            }
            catch (Exception excessao)
            {
                MessageBox.Show("Verifique o campo idade que deve ser numérico");
                return null;
            }
        }

        private void preencheDados(Pessoa p)
        {
            txtNome.Text = p.nome;
            txtIdade.Text = p.idade.ToString();
            txtCurso.Text = p.curso;
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            Pessoa p = recolheDados();
            if (p != null)
            {
                lista.Add(p);
                PessoaDAO dao = new PessoaDAO();
                dao.pessoa = p;
                dao.cadastrar();

                atualizaLista();
                txtNome.Text = "";
                txtIdade.Text = "";
                txtCurso.Text = "";
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtNome.Text = "";
            txtIdade.Text = "";
            txtCurso.Text = "";
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                int busca = int.Parse(txtBuscar.Text);
                if (busca < lista.Count)
                {
                    preencheDados(lista[busca]);
                }
                else
                {
                    MessageBox.Show("Pessoa não existe!");
                }
            }
            catch (Exception excessao)
            {
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Pessoa p = (Pessoa)lstPessoa.SelectedItem;
            if (p != null)
            {
                PessoaDAO dao = new PessoaDAO();
                dao.consultar(p.registro);
                preencheDados(p);
            }     
        }


        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnConsultar_Click(sender, e);
        }


        private void btnRemover_Click(object sender, EventArgs e)
        {
            Pessoa p = (Pessoa)lstPessoa.SelectedItem;
            if (p != null)
            {
                DialogResult resultado = MessageBox.Show("Deseja realmente remover?", "Aviso", MessageBoxButtons.YesNo);
                if (resultado == DialogResult.Yes)
                {
                    PessoaDAO dao = new PessoaDAO();
                    dao.pessoa = p;
                    dao.excluir();
                    lista.Remove(p);
                    atualizaLista();
                }
            }

            else
            {
                MessageBox.Show("Nenhum carro esta selecionado!");
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Pessoa p = (Pessoa)lstPessoa.SelectedItem;
            if (p != null)
            {
                Pessoa a = recolheDados();
                if (a != null)
                {
                    p.registro = a.registro;
                    p.nome = a.nome;
                    p.idade = a.idade;
                    p.curso = a.curso;
                    PessoaDAO dao = new PessoaDAO();
                    dao.pessoa = p;
                    dao.alterar();
                    atualizaLista();
                }
            }
        }

        private void btnCriarBD_Click(object sender, EventArgs e)
        {
            PessoaDAO dao = new PessoaDAO();
            dao.criardb();
        }
    }
}
